﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MeshData
{
    public string name;
    public Material material;
    public List<Vector3> vecticles;
    public int[] triangles;
    public Vector2[] uvs;
}
