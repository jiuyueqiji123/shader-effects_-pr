﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class BlendPlayer : MonoBehaviour
{

    public string blendSourceMesh;
    public string blendTargetMesh;
    public Material defaultMat;
    [Range(0,1)]
    public float blendValue = 0.5f;

    const string meshFolder = "Assets/Resources/shapes";

    MeshData src, dst;

    GameObject tempMeshObj;
    MeshRenderer tempRender;
    MeshFilter temFilter;
    Mesh mesh;

    void Start()
    {

        tempMeshObj = new GameObject("tempMesh");
        tempRender = tempMeshObj.AddComponent<MeshRenderer>();
        tempRender.material = defaultMat;
        temFilter = tempMeshObj.AddComponent<MeshFilter>();
        mesh = temFilter.mesh;

        LoadMesh();
        MeshSync();
    }

    private void LoadMesh()
    {
        string srcPath = Path.Combine(meshFolder, blendSourceMesh);
        string dstPath = Path.Combine(meshFolder, blendTargetMesh);
        src = JsonUtility.FromJson<MeshData>(File.ReadAllText(srcPath));
        dst = JsonUtility.FromJson<MeshData>(File.ReadAllText(dstPath));
    }

    List<Vector3> verticles,finalVerts;
    int[] triangleArr;
    private void MeshSync()
    {
        int counter = 0;
        int srcVertCount = 1;
        int vertPerVer = (dst.vecticles.Count - 1) / (src.vecticles.Count - 1);
        verticles = new List<Vector3>();

        //加入起始点
        verticles.Add(src.vecticles[0]);

        //加入余下的点。目标网格的点根据原始网格的顶点数进行分割
        for (int i = 1; i < dst.vecticles.Count; i++)
        {
            Vector3 vec = Vector3.zero;
            if(srcVertCount < src.vecticles.Count - 1)
            {
                vec = src.vecticles[srcVertCount+1] - src.vecticles[srcVertCount];
            }
            else
            {
                 vec = src.vecticles[1] - src.vecticles[srcVertCount];
            }
            Vector3 dir = vec.normalized;
            float avlen = vec.magnitude / vertPerVer;
            Vector3 targetPoint = src.vecticles[srcVertCount] + dir * avlen * counter;
            verticles.Add(targetPoint);

            if (counter >= vertPerVer)
            {
                counter = 0;
                srcVertCount++;    
            }
            
            counter++;
        }

        finalVerts = new List<Vector3>();
        verticles.ForEach(x=>finalVerts.Add(x));

        //准备三角形数组
        triangleArr = new int[3 * (verticles.Count - 1)];

        //绘制网格
        DrawMesh();

    }

    private void DrawMesh()
    {
        triangleArr = new int[3 * (verticles.Count - 1)];
        for (int i = 0,tri=0; i < verticles.Count; i++,tri+=3)
        {
            if (i < verticles.Count - 2)
            {
                triangleArr[tri] = 0;
                triangleArr[tri + 1] = i + 1;
                triangleArr[tri + 2] = i + 2;
            }
            else if (i == verticles.Count - 2)
            {
                triangleArr[tri] = 0;
                triangleArr[tri + 1] = i + 1;
                triangleArr[tri + 2] = 1;
            }

        }

        mesh.SetVertices(verticles);
        mesh.SetTriangles(triangleArr, 0);
    }

    void Update()
    {
        BlendOpt();
    }

    private void BlendOpt()
    {
        for (int i = 0; i < verticles.Count; i++)
        {
            finalVerts[i] = Vector3.Lerp(verticles[i],dst.vecticles[i],blendValue);
        }
        mesh.SetVertices(finalVerts);
        mesh.SetTriangles(triangleArr, 0);
    }
}
